# This is a Revit shared parameter file.
# Do not edit manually.
*META	VERSION	MINVERSION
META	2	1
*GROUP	ID	NAME
GROUP	1	Общие параметры
GROUP	4	Помещения
GROUP	5	Виды
GROUP	6	Листы
GROUP	7	Окна
GROUP	9	Трубопроводы
GROUP	10	Спецификация оборудования изделий и материалов
GROUP	11	Экспортированные параметры
GROUP	12	Стены / витражи
GROUP	13	Концепция
GROUP	14	Спецификации
GROUP	15	TM
GROUP	16	геометрические параметры
GROUP	17	Сборный ж. б.
*PARAM	GUID	NAME	DATATYPE	DATACATEGORY	GROUP	VISIBLE	DESCRIPTION	USERMODIFIABLE
PARAM	fff85e02-ac76-43a2-8e37-816971194de2	Назначение листа	TEXT		6	1	Указывает принадлежность листа комплекту для выдачи задания или финальной выдачи 	1
PARAM	bb3fbe03-6b93-46ad-bc7d-8a32953a377f	Тип пола	TEXT		4	1		1
PARAM	92b30e08-c16c-4aa9-8faf-8bf94cada559	Тип марка обозначение документа опросного листа	TEXT		10	1		1
PARAM	37969008-89a3-4602-aa62-68640d14926e	magi2DFilename	TEXT		11	0		1
PARAM	6661270a-487d-44af-9c67-1a5e97f666f9	Примечание в спецификацию	TEXT		10	1		1
PARAM	1fe08110-fab3-40d8-a7f6-39284551894b	кол_пластин	NUMBER		15	1		1
PARAM	d2e69c10-9d66-4155-9035-fd58288a9707	высота	LENGTH		16	1		1
PARAM	374f9612-0a65-4a2a-a830-680973dcc793	Витраж_Марка	TEXT		12	1		1
PARAM	ab409417-2d5e-4f75-aa6a-204cc4ee6f74	Боковой. Строка 1 должность	TEXT		6	1		1
PARAM	559c2c1b-39b6-4e93-8e06-efbdc321bc0f	Наименование и техническая характеристика	TEXT		10	1		1
PARAM	9c98831b-9450-412d-b072-7d69b39f4029	Обозначение	TEXT		1	1		1
PARAM	20e3931e-c22b-43ee-9256-9a2215b576a8	Боковой. Строка 3 фамилия	TEXT		6	1		1
PARAM	15284426-f3d8-4c55-9a6c-f9eea1393db3	Строка 3 фамилия	TEXT		6	1		1
PARAM	e313f126-7e51-4a5d-a45a-7c6dfe02124a	Назначение вида	TEXT		5	1		1
PARAM	a4e6bd2e-63b2-4ee3-88fa-8548e1438cc5	№ помещения	NUMBER		4	1		1
PARAM	37623a30-2630-454e-8e1e-6a3ef319063f	Жилая площадь	AREA		4	1		1
PARAM	c2f49234-25d3-4abc-8638-dde90c2527aa	Количество листов	INTEGER		6	1		1
PARAM	aa4e0636-b1b9-4ea6-8123-244a8679db0e	Боковой. Строка 2 должность	TEXT		6	1		1
PARAM	6ca82d36-9c55-450a-a479-002c4736cc06	Строка 2 фамилия	TEXT		6	1		1
PARAM	64f96838-ebe9-49d3-b1ff-bd8acd8b9228	площадь_теплообменника	AREA		15	1		1
PARAM	28c0133e-ee73-4f08-b6b1-1260213d8268	Единица Измерения	TEXT		10	1		1
PARAM	7a9f273e-9664-4950-9bca-3d96b2670ba6	Строка 1 должность	TEXT		6	1		1
PARAM	9745913e-c0fe-49fc-a54a-2e125dfbbd18	Масса_общ_в_кг	NUMBER		10	1		1
PARAM	00178540-addd-412e-83e5-76db04dbb325	Строка 6 фамилия	TEXT		6	1		1
PARAM	8a6e0145-58ea-4c2a-99b7-79e64e71f365	Год выпуска	INTEGER		6	1		1
PARAM	81f59847-bc5e-41a2-89f6-f840eaeb0258	Строка 6 должность	TEXT		6	1		1
PARAM	246b924b-608e-4ad6-8625-9cb10dfd83c7	Х	TEXT		14	1		1
PARAM	c0f4a14d-0a30-4e08-a233-2a1a23db2403	Строка 4 должность	TEXT		6	1		1
PARAM	5c411f4e-9e96-4b3c-871e-c7ec7fb5200a	длина	LENGTH		16	1		1
PARAM	141e0a57-d39a-459b-b83d-24cb6b5ebf24	Толщина стенки	PIPE_SIZE		9	1		1
PARAM	0425fa5d-6993-48a5-a2bd-be27096274f5	Формат и кратность	TEXT		6	1		1
PARAM	1ab4eb5f-2039-4037-ae15-982dda99ed36	гост_серия	TEXT		17	1		1
PARAM	9058b765-6d6b-4b30-9530-5f73bd74ed1d	8мм	TEXT		14	1		1
PARAM	889c4866-0229-4c75-965c-3a8164fabded	bxh	TEXT		1	1		1
PARAM	053e616e-cb3c-4dc5-8348-1c407e8e13a6	Строка 2 должность	TEXT		6	1		1
PARAM	6153ed75-b3d0-4e9a-ade5-f851a2a4cab6	Строка 5 фамилия	TEXT		6	1		1
PARAM	5242ae79-aee7-4f8e-af9e-e7de61f586e2	номер_оси	TEXT		1	1		1
PARAM	1168a97a-66a2-4f68-9dc7-d4af74c9ffe0	высота_отверстие	LENGTH		16	1		1
PARAM	d05c357c-e536-4225-914c-afc9f9a02ae0	Цвет по RAL	TEXT		1	1		1
PARAM	0a73157d-6885-4afc-8d0a-9529252477fb	диаметр	LENGTH		16	1		1
PARAM	73790284-e8c2-4717-a291-62460b2a596f	Боковой. Строка 2 фамилия	TEXT		6	1		1
PARAM	401a8a84-05e9-4efb-ae41-31b560235e7f	Код Справочника МТР	TEXT		10	1		1
PARAM	a856a784-03da-4238-9bd9-9881b9938b8a	Строка 1 фамилия	TEXT		6	1		1
PARAM	7d111c85-4759-424c-9694-ea7efb65e7c8	Огнестойкость	TEXT		4	1		1
PARAM	b6fd4087-fa35-4172-8ba8-a1fad1d2e61d	Этаж	NUMBER		1	1		1
PARAM	24a0dd8c-4f01-4137-8d03-2d78d3c1628b	Завод изготовитель	TEXT		10	1		1
PARAM	7f45f58c-1a5e-48f5-b292-bbf312c0bf2e	Строка 4 фамилия	TEXT		6	1		1
PARAM	7f0c8590-53a2-4ddf-aaed-c70eb9b687bc	Раздел проекта	TEXT		6	1		1
PARAM	369f0091-dc5e-4365-8ce0-695a8cff8586	Марка_перемычка	TEXT		1	1		1
PARAM	a3cafb97-e538-4761-95c9-16b2ff45abfe	объём_бетона_м.куб.	NUMBER		17	1		1
PARAM	36937fa3-0131-48b5-9a96-ab8276292084	Коэффициент площади	NUMBER		4	1		1
PARAM	a61521a4-5051-4e64-8e39-6d581128b6eb	ширина_отверстие	LENGTH		16	1		1
PARAM	98b64fa4-3091-4fa6-991b-615892bfae0e	axb	TEXT		1	1		1
PARAM	315022a6-f01a-489a-89d4-1b3538f8b8cd	ширина	LENGTH		16	1		1
PARAM	c9aadea6-4756-431e-9950-e9b6abfe55ed	Для установки в панель	YESNO		1	1		1
PARAM	cb667fab-c379-41b2-b5e1-b28bbc162524	назначение	TEXT		1	1		1
PARAM	3dc943bc-1b11-47e6-9cde-880d51a9d861	Строка 3 должность	TEXT		6	1		1
PARAM	aaf01dbd-8f92-4138-beee-e19498552ad1	отм_низа	NUMBER		16	1		1
PARAM	0e2040be-0d95-49d2-ab5d-b6654b04e801	глубина	LENGTH		16	1		1
PARAM	e64448c1-c3b7-46d5-8691-9f4936c63dbf	Масса единицы в кг	NUMBER		10	1		1
PARAM	e2f9bdc2-a9e6-47a3-b240-c953ae52f63a	Номер листа сквозной	TEXT		6	1		1
PARAM	370c38c6-c976-4460-9924-64c76015a814	вес_кг	NUMBER		17	1		1
PARAM	ca0521c7-fdb9-4868-8047-9e81c665c9c7	Боковой. Строка 3 должность	TEXT		6	1		1
PARAM	edd5f8cb-cb07-4bc2-8603-ff0045628f50	площадь_пластины	AREA		15	1		1
PARAM	e6e0f5cd-3e26-485b-9342-23882b20eb43	Наименование	TEXT		1	1		1
PARAM	62d888d3-73a3-4a9c-9037-7ebd22f7fc96	Категория помещения	TEXT		4	1		1
PARAM	597f72d5-01a8-416b-805c-a18cbbc49254	Боковой. Строка 1 фамилия	TEXT		6	1		1
PARAM	e166f8d8-b4ec-4488-a7a1-b7ee4ebdce1b	Владелец вида	TEXT		5	1		1
PARAM	88afefe1-87f0-4935-91f2-d89055e73802	Секция	NUMBER		1	1		1
PARAM	7e974de2-5e90-4af8-ba6a-601fe0c76b0f	Строка 5 должность	TEXT		6	1		1
PARAM	f88dc0e3-d3fe-427f-85ea-75010089b773	Отметка нуля	LENGTH		6	1		1
PARAM	1cb78ee8-7c5a-47c9-b4fc-378d5af0cf1b	Подоконник.Обозначение	TEXT		7	1		1
PARAM	2efaf5e9-8553-4585-978e-39992caf8922	Принадлежность_конструкции	TEXT		1	1	Введите марку конструкции для которой выполнен разрез	1
PARAM	62c95aeb-7a18-48f1-844e-c479be9f0844	расход_стали_кг	NUMBER		17	1		1
PARAM	10a1a9ef-9564-4351-a425-c2416c7a2436	Код обордования изделия материала	TEXT		10	1		1
PARAM	95903df6-ad24-4d3c-b9c1-af684575196e	Кем занято	TEXT		4	1		1
PARAM	93efb6fe-13d2-428d-a7f6-90af935ce5c3	Общая площадь	AREA		4	1		1
PARAM	84cfb87c-28c8-46e3-ace1-e6fc8cbf36da	Стадия	TEXT		6	1		1
PARAM	961eee92-208f-41e0-9a3c-abf4ee1aeb71	Месяц выпуска	TEXT		6	1		1
PARAM	505013c0-18c7-436c-b050-c4d4600fc295	Номер листа для выпуска	TEXT		6	1		1
PARAM	8eae28ca-6385-464e-b533-a3c346b33dcc	Всего листов	TEXT		6	1		1
PARAM	376dc279-95f5-4c2e-8ca1-d60c80f11ed3	Компания	TEXT		6	1		1
PARAM	7ddf8aac-f697-499f-8517-0e44295f9d72	Отметка нуля м	LENGTH		6	1	Абсолютная отметка нуля в метрах	1